CFLAGS := -g -Wall
LDFLAGS :=

PKGS := glib-2.0 gmyth gtk+-3.0 gmodule-2.0

CFLAGS += `pkg-config --cflags $(PKGS)`
LDFLAGS += `pkg-config --libs $(PKGS)`

mythface: mythface.c

.PHONY: clean

clean:
	rm -f mythface
